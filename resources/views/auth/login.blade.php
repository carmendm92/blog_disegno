<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-5">
                <h1 class="fw-bolder">Login</h1>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
       <div class="row justify-content-center align-items-center mt-5">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        <div class="col-12 col-md-4">
            <form method="POST" action="{{route('login')}}">
                @csrf
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Inserisci la tua Email</label>
                  <input type="email" class="form-control" name="email">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Inserisci la Password</label>
                  <input type="password" class="form-control" name="password">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-success mb-4">Login</button>
                </div>
              </form>
        </div>
        </div> 
    </div>
    
</x-layout>