<x-layout>
    <div class="container">
        <div class="row justify-content-center mt-5">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <div class="row justify-content-center align-items-center my-5">
            @foreach ($articles as $article) 

                <div class="col-12 col-md-6 col-xl-3 mt-3">
                    <x-card
                    titolo="{{$article->title}}"
                    categoria="{{$article->category}}"
                    autore="{{$article->user->name}}"
                    imagine="{{Storage::url($article->img)}}"
                    route="{{route('articolo.detail', compact('article'))}}"
                    >
                    </x-card>
                </div>
            @endforeach
        </div>
    </div>
    

</x-layout>