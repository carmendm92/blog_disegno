<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 justify-content-center container-form p-5">
                <h2 class="text-center fw-bolder fa-2x">Grazie per averci contattato, ti risponderemo al più presto.</h2>
            </div>
            <div class="col-12 text-center">
                <a href="{{route('homepage')}}" class="btn border border-dark bg-success fw-bolder mb-4">Torna alla home</a>
           </div>
        </div>
    </div>
</x-layout>