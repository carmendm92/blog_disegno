<x-layout>
    <div class="container-fluid">

        <div class="row justify-content-center mt-5">
            <div class="col-12 col-md-6"></div>
            <h1 class="text-center fw-bolder">{{$article->title}}</h1>
            <h5 class="card-title text-center">{{$article->category}}</h5>
        </div>

        <div class="row justify-content-center align-items-center">

            <div class="col-12 col-md-6 mt-5 p-4 border border-info">
                <p class="card-text ">{!! nl2br(e($article->body)) !!}</p> 
                <div class="col-12 text-center mb-2">
                    <p class="card-text text-end"><em>{{$article->user->name}}</em></p>
                </div>
            </div>

            <div class="col-12 col-md-4 m-5 p-4 border border-warning d-flex justify-content-center">
                <img src="{{Storage::url($article->img)}}" class="img-fluid" alt="{{$article->title}}">
            </div>

           

        </div> 

        <div class="row jusfify-content-center">
            @auth
                @if (Auth::user()->id ==$article->user->id)
                    <div class="col-6 text-end">
                        <a href="{{route('articolo.edit', compact('article'))}}" class="btn btn-success mt-2">Modifica articolo</a>
                    </div>
                    <div class="col-6">
                        <form action="{{route('cancella', compact('article'))}}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger mt-2">Elimina</button>
                        </form>
                    </div>
                @endif
            @endauth
        </div>
    </div>    
    <div class="container">
        <div class="row">
                <div class="col-12 text-end">
                    <a href="{{route('articoli')}}" class="btn btn-primary mt-2 mb-5">Indietro</a>
                </div>
            </div>
    </div>
        

    
</x-layout>