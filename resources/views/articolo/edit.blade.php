<x-layout>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-6 text-center m-4">
          <h1 class="fw-bolder">Modifica il tuo articolo</h1>
        </div>
      </div>
      @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
      @endif

      <div class="row justify-content-center">
        <div class="col-6 ">
          <form method="POST" action="{{route('articolo.update', compact('article'))}}"enctype="multipart/form-data">
            @method('PUT')
            @csrf
              <div class="mb-3">
                <label for="exampleInputtext1" class="form-label">Modifica titolo titolo</label>
                <input type="text" class="form-control border-warning" name="title" value="{{$article->title}}">
              </div>

              <div class="mb-3">
                <label for="exampleInputtext" class="form-label">Modifica la categoria</label>
                <input type="text" class="form-control border-success" name="category" value="{{$article->category}}"> 
              </div>

              <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">Inserisci l'articolo</label>
                <textarea class="form-control border-primary" id="exampleFormControlTextarea1" name="body" rows="3">{{$article->body}} </textarea>
              </div>

              <div class="mb-3 text-center">
                <label for="exampleInputEmail1" class="form-label ">La tua immagine</label>
                <img src="{{Storage::url($article->img)}}" class="border border-info p-1" widith="200px" height="200px" alt="{{'$article->title'}}">
              </div>

              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Inserisci una nuova immagine</label>
                <input type="file" class="form-control border border-danger" id="exampleInputEmail1" name="img">
              </div>

              <div class="row justify-content-center mb-5">
                <div class="col-6 text-end">
                  <button type="submit" class="btn btn-success">Invia articolo</button>
                </div>

              <div class="col-6">
                <a href="{{route('articoli')}}" class="btn btn-primary">Annulla</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</x-layout>