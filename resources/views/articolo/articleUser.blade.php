<x-layout>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <h1 class="fw-bolder text-center">{{Auth::user()->name}} i tuoi articoli</h1>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <div class="row justify-content-center align-items-center my-5">
            @foreach ($articles as $article) 

                <div class="col-12 col-md-6 col-xl-3 mt-3">
                    <x-card
                    titolo="{{$article->title}}"
                    categoria="{{$article->category}}"
                    autore="{{$article->user->name}}"
                    imagine="{{Storage::url($article->img)}}"
                    route="{{route('articolo.detail', compact('article'))}}"
                    >
                    </x-card>
                </div>
            @endforeach
        </div>
    </div>

    <div class="container">
       <div class="row">
            <div class="col-12 text-end">
                <form action="{{route('user.delete', ['user'=>Auth::user()])}}" method="POST">
                    @csrf
                    @method("delete")
                    <div class="mt-4">
                        <button type="submit" class="btn border border-dark bg-danger fw-bolder mb-4">Elimina Profilo</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    

</x-layout>


