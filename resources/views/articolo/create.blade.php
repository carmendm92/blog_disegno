<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 text-center m-4 ">
                <h1 class="fw-bolder">Inserisci il tuo articolo</h1> 
            </div>
            @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <form method="POST" action="{{route('articolo.store')}}"enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputtext1" class="form-label">Inserisci il titolo</label>
                      <input type="text" class="form-control border-warning" name="title">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputtext" class="form-label">Inserisci la categoria</label>
                        <input type="text" class="form-control border-success" name="category">
                      </div>
                     <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">inserisci l'articolo</label>
                        <textarea class="form-control border-primary" id="exampleFormControlTextarea1" name="body" rows="3">{{old('message')}}</textarea>
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">inserisci immagine</label>
                        <input type="file" class="form-control border-danger" id="exampleInputEmail1" name="img">
                     </div>
                    <div class="mb-4 text-center ">
                        <button type="submit" class="btn btn-info">Invia articolo</button>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</x-layout>