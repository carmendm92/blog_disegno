<footer class="container-fluid footer d-flex justify-content-center align-items-center">
  
        <div class="row text-center h-100">

            <div class="col-12 ">
                <h3 class="fw-bolder">Chi siamo</h3>
                <h4>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Blanditiis voluptate, suscipit.</h4>
            </div>

            <div class="col-12 mt-4 ">
                <div class="container-icon d-flex justify-content-center">
                    <a class="nav-link " href="#">
                        <i class="fa-brands fa-facebook-f link-footer-facebook link-footer fa-2x"></i>
                    </a>
                    <a class="nav-link " href="#">
                        <i class="fa-brands fa-twitter link-footer-twitter link-footer fa-2x"></i>
                    </a>
                    <a class="nav-link " href="#">
                        <i class="fa-brands fa-pinterest-p link-footer link-footer-pinterest fa-2x"></i>
                    </a>
                    <a class="nav-link" href="#">
                        <i class="fa-brands fa-instagram link-footer-instagram link-footer fa-2x"></i>
                    </a>
                </div>
            </div>
        </div>
</footer>