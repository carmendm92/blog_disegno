<div class="p-1 border border-success d-flex justify-content-center align-items-center flex-column">
    <div class="card">
        <div class="card-header bg-primary">
           {{$titolo}}
        </div>
    </div>
        <h4 class="mt-2">{{$categoria}}</h4>
        <img class="p-4 text-center"src="{{$imagine ?? ''}}" alt="{{$titolo}}" widith="200px" height="200px">
        <p class="card-text">{{$corpo ?? ''}}</p>
        <h5 class="card-title text-center p-2">Autore <br><em>{{$autore}}</em></h5>
        <a class="btn border border-dark button_card bg-warning" href="{{$route ?? ''}}">Leggi altro</a>
  </div>