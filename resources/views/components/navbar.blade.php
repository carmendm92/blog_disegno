<nav class="navbar navbar-expand-lg bg-light fixed-top">
    <div class="container-lg">
      <a class="navbar-brand fw-bolder text-dark" href="{{route('homepage')}}">Creativity</a>
      <button class="navbar-toggler text-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span><i class="fa-solid fa-bars"></i></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav m-auto">
          <li class="nav-item mx-3">
            <a class="nav-link link-navbar" aria-current="page" href="{{route('homepage')}}">Home</a>
          </li>
          <li class="nav-item mx-3">
            <a class="nav-link link-navbar" aria-current="page" href="{{route('contattaci')}}">Contattaci</a>
          </li>
          @guest
          <li class="nav-item">
            <a class="nav-link link-navbar mx-3" href="{{route('register')}}">Registrati</a>
          </li>
          <li class="nav-item">
            <a class="nav-link link-navbar mx-3" href="{{route('login')}}">Login</a>
          </li>
        
            @endguest

          @auth
          <li>
            <a class="nav-link link-navbar mx-3" href="{{route('articolo.create')}}">Inserisci Articolo</a>
          </li>
         
        </ul>
        <div class="container-pannel-user" id="navbarSupportedContent">
        <ul class="navbar-nav m-auto">
          <li class="nav-item dropdown mx-3">
            <a class="nav-link dropdown-toggle link-navbar" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Benvenuto {{Auth::user()->name}}
            </a>
          <ul class="dropdown-menu " aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item uppercase" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('form-logout').submit();">Logout</a></li>
            <form action="{{route('logout')}}" method="POST" id="form-logout">
              @csrf
            </form>
            <li>
              <a class="dropdown-item uppercase" href="{{route('articolo.user')}}">I miei articoli</a>
            </li>
          </ul>
        </li>
        </ul>
        @endauth
      </div>
      </div>
      
    </div>
  </nav>