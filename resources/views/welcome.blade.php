<x-layout>
    <div class="container-fluid">
        <div class="row justify-content-center">
          @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
          @endif
            <div class="col-12 col-md-3 my-5 p-4">
              <div class="p-2 border border-dark">
                  <div class="card text-center">
                      <div class="card-header bg-danger ">
                         Artcoli inseriti da voi
                      </div>
                  </div> 
                   <h5 class="card-title p-2 fw-bolder text-center">Voi creativi</h5>
                      <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis vel rerum.</p>
                      <a class="btn border border-dark button_enter" href="{{route('articoli')}}">Entra</a>
                </div>
            </div>
            
            <div class="col-12 col-md-9 maxy-card d-flex justify-content-center align-items-center">
              <div class=" p-4 ">
                <p>Benvenuti nel blog della creatività, una community dove scrivere articoli, mostrare lavori, aiutarci e creare</p>
              </div>
            </div>
        </div>
    </div>
      
  
</x-layout>