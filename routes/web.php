<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('homepage');

Route::get('/articles', [PublicController::class, 'Article'])->name('articoli');

Route::get('/create', [ArticleController::class, 'Create'])->name('articolo.create');

Route::post('/store', [ArticleController::class, 'Store'])->name('articolo.store');

Route::get('/dettaglio/articolo/{article}', [PublicController::class, 'Show'])->name('articolo.detail');

Route::delete('/delete/articolo/{article}', [ArticleController::class, 'Delete'])->name('cancella');

Route::get('/edit/articolo/{article}', [ArticleController::class, 'Edit'])->name('articolo.edit');

Route::put('/modifica-articolo/{article}/update', [ArticleController::class, 'Update'])->name('articolo.update');

Route::get('/i-miei-articoli', [ArticleController::class, 'ArticleUser'])->name('articolo.user');

Route::get('/contattaci', [FormController::class, 'Form'])->name('contattaci');

Route::post('/contattaci/submit', [FormController::class, 'Box'])->name('box');

Route::get('/grazie', [FormController::class, 'Thanks'])->name('grazie');

Route::delete('/utente/{user}', [ArticleController::class, 'deleteUser'])->name('user.delete');