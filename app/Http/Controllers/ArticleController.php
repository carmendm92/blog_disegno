<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\PutRequest;
use App\Http\Requests\StoreRequest;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function Create(){
        return view('articolo.create');
    }
    
    public function Store(StoreRequest $req){
        Auth::user()->articles()->create([
            'title'=>$req->title,
            'category'=>$req->category,
            'body'=>$req->body,
            'img'=>$req->file('img')->store('public/img')
        ]);
        return redirect (route('articoli'))->with('status', 'Articolo inserito');
    }

    public function Delete(Article $article){
        if(Auth::id() == $article->user->id){
        $article->delete();
        return redirect (route('articoli'))->with('status', 'Il tuo articolo è stato cancellato');
        }
    }

    public function Edit(Article $article){
        
        return view('articolo.edit', compact('article'));

    }

    public function Update(PutRequest $req, Article $article){
    if(Auth::id() == $article->user->id){
        if($req->file('img')){

            $article->update([
                'title'=>$req->title,
                'category'=>$req->category,
                'body'=>$req->body,
                'img'=>$req->file('img')->store('public/img')
            ]);
        }
            $article->update([
                'title'=>$req->title,
                'category'=>$req->category,
                'body'=>$req->body,
            ]);
        }
        return redirect (route('articolo.detail', $article))->with('status', 'Il tuo articolo è stato modificato');
    }

    public function ArticleUser(){
        $articles = Auth::user()->articles()->get();
        return view('articolo.articleUser', compact('articles'));
    }

    public function deleteUser(User $user){
        foreach ($user->articles as $article){
            $article->user()->dissociate();
            $article->user()->associate(1);
            $article->save();
        }
        $user->delete();

        return redirect (route('homepage', $user))->with('status', 'il tuo profilo è stato eliminato');
    }
}