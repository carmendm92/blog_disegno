<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\PublicController;

class PublicController extends Controller
{
    public function home(){
            return view('welcome');
        }

    public function Article(){
    $articles = Article::all();
        return view('articles', compact('articles'));
    }

    public function Show(Article $article){
        return view('articolo.detail', compact('article'));
    }
}

