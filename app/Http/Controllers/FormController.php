<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Contact;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Requests\BoxRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\FormController;

class FormController extends Controller

{
   public function Form(){
       return view('contattaci');
   }

   public function Box (BoxRequest $req){
    
        $contact = Contact::create([
            'email'=>$req->email,
            'name'=>$req->name,
            'message'=>$req->message
        ]);
        
        Mail::to($contact->mail)->send(new ContactMail($contact));

        return redirect (route ('grazie'));
   }
   
   public function Thanks(){
       return view('thankPage');
   }
}
